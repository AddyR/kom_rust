use tower_http::cors::{Any, CorsLayer};
use axum::{
    routing::get,
    Router,
};

pub struct ServerHTTP {
    ip: String,
    port: u32,
    // max_workers: Option<i32>,
    // mode: Option<String>,
    // executor: Option<Executor>,
    address: String,
    sse:Option<bool>,
    pub listener: Option<tokio::net::TcpListener>,
    pub app: Option<Router>,
}

impl<'a> ServerHTTP{

    pub async fn new(
        port: Option<u32>,
        ip:Option<String>,
    ) -> ServerHTTP {

        // intializing server
        let server = ServerHTTP {
            ip: ip.unwrap_or_else(|| "127.0.0.1".to_string()), 
            port: port.unwrap_or_else(|| 8000), 
            sse: Some(false),
            address:"".to_string(),
            listener: None,
            app: None,
        };

        server
    }

    // fn set_api(ip_address: String, port: u16) -> (tokio::net::TcpListener, Router) {
    pub async fn set_api(&mut self) {
        self.address = format!("{}:{}", self.ip, self.port);

        
        // cors middleware
        let cors_middleware = CorsLayer::new()
        .allow_methods(Any)
        .allow_origin(Any)
        .allow_headers(Any);
    
    
        self.app = Some(Router::new().
            route("/", get(|| async { "Hello, World!" }))
            .layer(
            cors_middleware
            )
        );
        
        self.listener = Some(tokio::net::TcpListener::bind(&self.address
            ).await.unwrap());
    }

}