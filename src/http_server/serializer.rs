use serde::{Serialize, Deserialize};
use serde_json::{Value, json};


// Define a trait for serialization.
trait Serializer {
    fn serialize(&self, x: Value, mode: &str) -> Value;
    fn deserialize(&self, x: Value) -> Value;
}