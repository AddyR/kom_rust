
mod http_server; // Import the module

use crate::http_server::server::ServerHTTP;

#[tokio::main]
async fn main() {
    let ip_address = "0.0.0.0".to_string();
    let port = 3001;

    let mut server_http = ServerHTTP::new(Some(port), Some(ip_address)).await;
    server_http.set_api().await;

    if let (Some(listener), Some(app)) = (server_http.listener, server_http.app) {
        axum::serve(listener, app).await.unwrap();
    } 
    else {
        eprintln!("Failed to initialize listener and app.");
    }
}